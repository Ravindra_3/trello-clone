import React from 'react';
import logo from './logo.svg';
import './App.css';
import Boards from './Component/Board';
import'bootstrap/dist/css/bootstrap.css';
import List from './Component/ListData'
import { BrowserRouter,Route ,Switch } from 'react-router-dom';
import { FaTrello ,} from 'react-icons/fa';


function App() {
  return (
   
   <BrowserRouter>

<div className="App">
 
   
<h1 className='title'><FaTrello/> Trello</h1>
   {/* <Boards/> */}
     <Switch>
     <Route path='/' exact component ={Boards}/>
     <Route path='/board-list/:id' component={List}/>
    

     </Switch>
<Route/>


    </div>
   </BrowserRouter>
  
  );
}

export default App;
