import React, { Component } from "react";
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import Model from "./Model";
import { FaHighlighter, FaPenFancy } from "react-icons/fa";
// import { FaHighlighter,FaPenFancy } from "react-icons/fa";
class Cards extends Component {
  state = {
    card: [],
    data: this.props.data,
    Name: this.props.data.name,
    showinput: false,
    showmodel: false,
    edit: false,
  };

  update() {
    this.setState({
      showinput: !this.state.showinput,
    });
  }

  update1 = (cardid, newname) => {
    // console.log(this.state.data)
    const newname1 = newname.target.parentNode.firstChild.value;
    const cardid1 = cardid;
    fetch(
      `https://api.trello.com/1/cards/${cardid}?name=${newname1}&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`,
      { method: "PUT" }
    )
      .then((e) => {
        this.setState({
          Name: newname1,
        });
      })
      .then((e) => {
        console.log("done" + this.state.Name);
      });
  };

  showmodel = (e) => {
    // e.stopImmediatePropagation();
    // e.preventdefulit()

    this.setState({
      showmodel: !this.state.showmodel,
    });
  };
  Model(e) {
    console.log(e);

    return <Model data={e} />;
  }

  showedinputbox = () => {
    console.log("bijbj");
    this.setState({
      edit: !this.state.edit,
    });
  };

  render() {
    return (
      <div className="list-group">
        {/* <h3> Lets go for a < FaHighlighter/>  </h3> */}

        {!this.state.showinput && (
          <div
            className="card1"
            id={this.state.data.id}
            onClick={(e) => this.showmodel(e)}
          >
            {this.state.Name}{" "}
            <div className ='cardedit'>
            <button
              type="button"
              className=" FaPenFancy"
              onClick={(e) => {
                e.preventDefault();
                e.stopPropagation();
                this.showedinputbox();
              }}
              
            >
              {" "}
              <FaPenFancy />{" "}
            </button>{" "}
            </div>
          </div>
        )}
        {!this.state.showinput && this.state.edit && (
          <span>
            <button
              type="button"
              onClick={(e) => {
                this.props.delete1(this.state.data.id);
              }}
              className="delete "
            >
              delete
            </button>
            <button
              type="button"
              onClick={(e) => {
                this.update(e);
              }}
              className="delete "
            >
              edit
            </button>
          </span>
        )}
        {this.state.showinput && (
          <div className="inputlist">
            <input
              type="text"
              className="form-control"
              placeholder={this.state.Name}
            ></input>
            <button
              type="button"
              onClick={(e) => {
                this.update1(this.state.data.id, e);
                this.update(e);
              }}
              className="btn btn-primary"
            >
              submit
            </button>
            <button
              type="button"
              onClick={(e) => this.update(e)}
              className="btn btn-primary"
            >
              cancel
            </button>
          </div>
        )}
        <div>{this.state.showmodel && this.Model(this.state.data)}</div>
      </div>
    );
  }
}

export default Cards;
