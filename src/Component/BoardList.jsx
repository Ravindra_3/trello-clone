import React, { Component } from 'react';
import {Link} from 'react-router-dom'


class Boardlist extends Component {
     componentDidMount(){

        // console.log(this.props)
     } 

 
    render() { 
      
        return ( 

<div className="card" >
 <div className="card-body">
        <h5 class="card-title" id={this.props.id}>{this.props.name}</h5>
        <Link to={`/board-list/${this.props.id}`} className="card-link" > Boards</Link>
        </div>
        </div>
  
         );
    }
}
// ${this.props.id}
 
export default Boardlist;


