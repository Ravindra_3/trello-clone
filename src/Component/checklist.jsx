import React, { Component } from "react";
import Checkitem from "./checkitems";
import { FaTrash } from "react-icons/fa";

class Checklist extends Component {
  state = {
    data: this.props.data,
    checkItems: this.props.data.checkItems || [],
    showinput: false,
  };

  showinput() {
    this.setState({
      showinput: !this.state.showinput,
    });
  }

  deletechecklist = (idChecklist, itemid) => {
    console.log(idChecklist + "hiui");
    console.log(itemid + "nk n njinjinijnbijbn csccscc");
    fetch(
      `https://api.trello.com/1/checklists/${idChecklist}/checkItems/${itemid}?key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`,
      {
        method: "DELETE",
      }
    )
      .then((response) => {
        return response;
      })
      .then((data) => {
        console.log(data + "done");

        this.setState({
          checkItems: this.state.checkItems.filter((item) => {
            return item.id != itemid;
          }),
        });
      });
  };
  addnewitemn(e) {
    const name = e.target.parentNode.firstChild.value;
    //  console.log('hii'+name+this.state.data.id)
    fetch(
      `https://api.trello.com/1/checklists/${this.state.data.id}/checkItems?name=${name}&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`,
      { method: "POST" }
    )
      .then((e) => {
        return e.json();
      })
      .then((e) => {
        console.log(e + "onijn");
        this.setState({
          checkItems: this.state.checkItems.concat([e]),
        });
      });
  }

  createcheckItems = () => {
    console.log(this.state.data);
    return this.state.checkItems.map((e) => {
      return (
        <Checkitem
          data={e}
          name={e.name}
          deletechecklist={this.deletechecklist}
        />
      );
    });
  };
  render() {
    return (
      <div>
        <ul className="list-group list-group-flush">
          {/* <h5>{this.state.data.name} <button type='button' className=' FaTrash' onClick={e=>{
               e.preventDefault()
              this.props.delete(this.props.listid)}}> < FaTrash/>  </button> </h4> </h5>
 */}

          <h4 className="list active">
            {this.state.data.name}{" "}
            <button
              type="button"
              className=" FaTrash"
              onClick={(e) => {
                e.preventDefault();
                this.props.delete(this.state.data.id);
              }}
            >
              {" "}
              <FaTrash />{" "}
            </button>{" "}
          </h4>
          {this.createcheckItems()}
          {!this.state.showinput && (
            <button
              type="button"
              onClick={(e) => {
                e.preventDefault();
                this.showinput(e);
              }}
              className=" list active addbution "
            >
              add item
            </button>
          )}
          {this.state.showinput && (
            <div className="inputlist">
              <input
                type="text"
                className="form-control"
                placeholder="enter checkitem"
              ></input>
              <button
                type="button"
                onClick={(e) => {
                  e.preventDefault();
                  this.showinput(e);
                  this.addnewitemn(e);
                }}
                className="btn btn-primary"
              >
                submit
              </button>
              <button
                type="button"
                onClick={(e) => {
                  e.preventDefault();
                  this.showinput(e);
                }}
                className="btn btn-primary"
              >
                cancel
              </button>
            </div>
          )}
        </ul>
      </div>
    );
  }
}

export default Checklist;
