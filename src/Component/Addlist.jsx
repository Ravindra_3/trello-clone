import React, { Component } from "react";
import { FaBeer } from "react-icons/fa";
// import Input from './input'

class Addlist extends Component {
  state = {
    name: "",
    input: true,
  };

  showinput = () => {
    this.setState({
      input: !this.state.input,
    });
  };

  addnewlist = (e) => {
    const name = e.target.parentNode.firstChild.value;
    const id = this.props.id;

    fetch(
      `https://api.trello.com/1/boards/${id}/lists?name=${name}&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`,
      { method: "POST" }
    )
      .then((data) => data.json())
      .then((x) => {
        console.log(x);
        this.setState({
          newlist: x.name,
        });
      })
      .catch((err) => console.log(err));

    console.log("done");
    this.setState({
      input: !this.state.input,
    });
  };

  render() {
    // console.log('hii')
    return (
      <div className="form-group">
        {this.state.input && (
          <button
            type="button"
            onClick={this.showinput}
            className=" list active btn btn-primary "
          >
            + add list
          </button>
        )}

        {!this.state.input && (
          <div className="inputlist">
            <input type="text" className="form-control"></input>
            <button
              type="button"
              onClick={(e) => this.addnewlist(e)}
              className="btn btn-primary"
            >
              submit
            </button>
            <h3>
              {" "}
              Lets go for a <FaBeer />?{" "}
            </h3>
          </div>
        )}
      </div>
    );
  }
}

export default Addlist;
