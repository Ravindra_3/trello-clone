import React, { Component } from "react";
import Cards from "./Cards";
import { FaTrash } from "react-icons/fa";

class Listname extends Component {
  state = {
    showinput: false,
    data: this.props.data.cards || [],
  };
  delete = (e) => {
    console.log(e);
    const del = e;

    fetch(
      `https://api.trello.com/1/cards/${del}?key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`,
      { method: "DELETE" }
    ).then((e) => {
      this.setState({
        data: this.state.data.filter((e) => {
          return e.id != del;
        }),
      });
    });

    this.setState({
      data: this.state.data.filter((e) => {
        return e.id != del;
      }),
    });
  };

  cards(props) {
    // console.log(props)
    return props.map((card) => {
      return <Cards data={card} delete1={this.delete} update={this.update} />;
    });
  }
  addnewcard = (e) => {
    const name = e.target.parentNode.firstChild.value;

    // console.log(name)
    fetch(
      `https://api.trello.com/1/cards?idList=${this.props.listid}&name=${name}&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`,
      { method: "POST" }
    )
      .then((e) => {
        return e.json();
      })
      .then((e) => {
        // console.log(e)
        this.setState({
          data: this.state.data.concat([e]),
          showinput: !this.state.showinput,
        });
      });
  };
  showinput = (event) => {
    this.setState({
      showinput: !this.state.showinput,
    });
    event.preventDefault();
  };

  render() {
    return (
      <div className="list-box">
        <h3 className="list active1">
          {this.props.name}{" "}
          <button
            type="button"
            className=" FaTrash"
            onClick={(e) => {
              e.preventDefault();
              this.props.delete(this.props.listid);
            }}
          >
            <FaTrash />
          </button>
        </h3>

        <h5>{this.cards(this.state.data)}</h5>
        {!this.state.showinput && (
          <button
            type="button"
            onClick={(e) => {
              e.preventDefault();
              this.showinput(e);
            }}
            className=" list active addbution "
          >
            add card
          </button>
        )}
        {this.state.showinput && (
          <div className="inputlist">
            <input type="text" className="form-control"></input>
            <button
              type="button"
              onClick={(e) => {
                e.preventDefault();
                this.addnewcard(e);
              }}
              className="btn btn-primary"
            >
              submit
            </button>
            <button
              type="button"
              onClick={(e) => {
                e.preventDefault();
                this.showinput(e);
              }}
              className="btn btn-primary"
            >
              cancel
            </button>
          </div>
        )}
      </div>
    );
  }
}

export default Listname;
