import React, { Component } from "react";
import Listname from "./lists";
import Addlist from "./Addlist";
import { FaHighlighter, FaPenFancy, FaCheck, FaTimes } from "react-icons/fa";

class List extends Component {
  state = {
    data: [],
    newlist: "",
    input: true,
  };
  showinput = () => {
    this.setState({
      input: !this.state.input,
    });
  };

  addnewlist = (e) => {
    const id = this.props.match.params.id;

    const name = e.target.parentNode.firstChild.value;
    //  const id=(this.props.id)

    fetch(
      `https://api.trello.com/1/boards/${id}/lists?name=${name}&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`,
      { method: "POST" }
    )
      .then((data) => data.json())
      .then((x) => {
        console.log(x);
        this.setState({
          input: !this.state.input,
          data: this.state.data.concat([x]),
        });
      })
      .catch((err) => console.log(err));

    console.log("done");
  };

  componentDidMount(props) {
    const id = this.props.match.params.id;

    // const fetch = require('node-fetch');

    fetch(
      `https://api.trello.com/1/boards/${id}/lists?cards=all&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`,
      {
        method: "GET",
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((text) => {
        console.log(text);
        this.setState({
          data: text,
        });
      })
      .catch((err) => console.error(err));
  }

  deletelist = (listid) => {
    console.log(listid);

    fetch(
      `https://api.trello.com/1/lists/${listid}/closed?value=true&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`,
      {
        method: "PUT",
      }
    )
      .then((response) => {
        console.log(`Response: ${response.status} ${response.statusText}`);
        return response.json();
      })
      .then((text) => {
        this.setState({
          data: this.state.data.filter((list) => {
            return list.id != listid;
          }),
        });
      })
      .catch((err) => console.error(err));
  };

  List() {
    console.log(this.state.data);

    return this.state.data.map((data) => {
      return (
        <Listname
          name={data.name}
          listid={data.id}
          data={data}
          delete={this.deletelist}
        />
      );
    });
  }

  render() {
    return (
      <div className="container">
        <div className="Alllists" id="container">
          <div className="list-group">
            <span className="list" id="lists">
              {this.List()}
            </span>
          </div>
          {/* <Addlist id={this.props.match.params.id}/> */}
          <div className="form-group">
            {this.state.input && (
              <button
                type="button"
                onClick={this.showinput}
                className=" list active btn btn-primary "
              >
                + add list
              </button>
            )}

            {!this.state.input && (
              <div className="inputlist">
                <input type="text" className="list listinput"></input>
                <button
                  type="button"
                  onClick={(e) => {
                    e.preventDefault();

                    this.addnewlist(e);
                  }}
                  className="btn btn-primary"
                >
                  submit
                </button>
                {/* this.addnewlist(e)}}  */}
                <button
                  className="btn btn-primary"
                  onClick={(e) => {
                    e.preventDefault();
                    this.showinput();
                  }}
                >
                  {" "}
                  <FaTimes />
                </button>
              </div>
            )}
          </div>
        </div>
      </div>
    );
  }
}

export default List;
