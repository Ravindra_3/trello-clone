import React, { Component, Fragment } from "react";
import ReactDOM from "react-dom";
import Modal from "react-modal";
import Checklist from "./checklist";
import {
  FaHighlighter,
  FaPenFancy,
  FaCheck,
  FaTimes,
  FaTrash,
} from "react-icons/fa";

class Model extends Component {
  state = {
    isActive: true,
    data: this.props.data,
    checklist: [],
    showinput: false,
  };

  showinput() {
    this.setState({
      showinput: !this.state.showinput,
    });
  }

  componentWillMount() {
    Modal.setAppElement("body");
  }

  componentDidMount() {
    console.log(this.state.data.id);
    fetch(
      `https://api.trello.com/1/cards/${this.state.data.id}/checklists?key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`,
      { method: "GET" }
    )
      .then((e) => {
        return e.json();
      })
      .then((e) => {
        this.setState({
          checklist: e,
        });
        console.log(this.state.checklist);
      });
  }

  togglemodal = () => {
    this.setState({
      isActive: !this.state.isActive,
    });
  };

  addchecklist = (e) => {
    const name = e.target.parentNode.firstChild.value;
    const id = this.state.data.id;

    fetch(
      `https://api.trello.com/1/checklists?idCard=${id}&name=${name}&key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`,
      { method: "POST" }
    )
      .then((e) => {
        return e.json();
      })
      .then((e) => {
        console.log(e);
        this.setState({
          checklist: this.state.checklist.concat([e]),
        });
      });
  };
  delete = (e) => {
    console.log(e);

    fetch(
      `https://api.trello.com/1/checklists/${e}?key=bee62d72c576ca71fa21264373bcbfbd&token=85a7e353fa9263959598f9375e4c31d6ee5356885d6127385408e067f4f025d5`,
      {
        method: "DELETE",
      }
    )
      .then((response) => {
        return response.json();
      })
      .then((text) => {
        console.log(text);
        this.setState({
          checklist: this.state.checklist.filter((x) => {
            return x.id != e;
          }),
        });
      })
      .catch((err) => console.error(err));
  };

  createchecklist = () => {
    console.log(this.state.data);

    return this.state.checklist.map((item) => {
      console.log(item);
      return (
        <Checklist
          data={item}
          addchecklist={this.addchecklist}
          delete={this.delete}
        />
      );
    });
  };

  render() {
    return (
      <section>
        <Modal isOpen={this.state.isActive}>
          <button onClick={this.togglemodal} className="btn btn-primary">
            <FaTimes />
          </button>
          <div className="maindiv">
            {/* <button onClick={this.togglemodal}>closemodal</button> */}

            <div className="submodal">
              <h1>{this.state.data.name}</h1>

              <div class="container">
                <ul class="list-group">
                  <li class="list-group-item active">checklist</li>
                  <div>
                    {this.createchecklist()}
                    {!this.state.showinput && (
                      <button
                        onClick={(e) => {
                          // this.checklist(this.state.data)
                          // this
                          this.showinput(e);
                        }}
                        className="btn btn-primary"
                      >
                        {" "}
                        + checklist
                      </button>
                    )}

                    {this.state.showinput && (
                      <div className="inputlist">
                        <input
                          type="text"
                          className="form-control"
                          placeholder="enter checkitem"
                        ></input>
                        <button
                          type="button"
                          onClick={(e) => {
                            e.preventDefault();
                            this.showinput(e);
                            this.addchecklist(e);
                          }}
                          className="btn btn-primary"
                        >
                          submit
                        </button>
                        <button
                          type="button"
                          onClick={(e) => {
                            e.preventDefault();
                            this.showinput(e);
                          }}
                          className="btn btn-primary"
                        >
                          cancel
                        </button>
                      </div>
                    )}
                  </div>
                </ul>
              </div>
            </div>
          </div>
        </Modal>
      </section>
    );
  }
}

export default Model;
